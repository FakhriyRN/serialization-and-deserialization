﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace DemoApplication
{
    [Serializable]
    public class IDnumber
    {
        public int ID;
    }

    [Serializable]
    public class Mahasiswa : IDnumber
    {
        public String Name;

        public Mahasiswa()
        {
            ID = 27;
            Name = "Fakhriy";
        }
    }

    class Program
    {    
        static void Main(string[] args)
        {
            IDnumber obj = new Mahasiswa();


            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(@"C:\Users\ASUS\Documents\VSCode\ExampleNew.txt",FileMode.Create,FileAccess.Write);

            formatter.Serialize(stream, obj);
            stream.Close();

            stream = new FileStream(@"C:\Users\ASUS\Documents\VSCode\ExampleNew.txt",FileMode.Open,FileAccess.Read);
            formatter = new BinaryFormatter();
            
            Mahasiswa objnew = (Mahasiswa)formatter.Deserialize(stream);

            Console.WriteLine(objnew.ID);
            Console.WriteLine(objnew.Name);

            Console.ReadKey();
        }
    }
}